(* Properties evaluated in this file: Individual Verifiability. *)
(* This specification models 2 CCRs (Return code control components), 2CCMs (Mixing control components), 4 voting options and voters selecting 2 voting options *)
(* Trust assumptions: *)
(* CCR1 and Alice are trustworthy *)
(* CCR2 and the voting server and the voting client and CCM1 and CCM2 are untrustworthy *)
(* RESULT *)
(* Individual Verifiability property is verified *)
(* i.e. if a voter ends her role (HappyUser(.)) then a ballot containing her intended vote(s)
has been inserted and confirmed by the honest CCR. *)

(* 1. Objects and Types *)

type agent_id. fun t_agent_id(agent_id) : bitstring [data,typeConverter]. (* The type for any IDs like the voting card ID. *)
type password. fun t_password(password) : bitstring [data,typeConverter]. (* The type for user passwords. *)
type num. fun t_num(num) : bitstring [data,typeConverter]. (* The type for natural numbers. *)
free c : channel. (* Public Channel for communication with the adversary # public *)
free Ch1 : channel. (* Channel between Alice and her (dishonest) voting client # public *)
free SC_CCRChannel : channel. (* Communication between the Setup Component (SC) and the honest CCR # (public) *)
free AliceSCChannel : channel. (* Channel in which Alice sends her pseudonymous identity idA to the Setup Component (SC) # (public) *)
free UserChannel : channel. (* Channel in which the Setup Component receives Alice's pseudonymous identity idA # (public) *)
fun v(num) : bitstring [data]. (* The function mapping the voter's selections to voting options # public , invertible *)
free idA : agent_id. (* Defines an honest voter Alice # public *)
free ja1,ja2,ja3,ja4 : num. (* Voting choices # public *)

(* 2 CCRs *)
free CCRId1,CCRId2: num.

fun H1(bitstring) : bitstring. (* The hash function (RecursiveHash(.) in the system specification) # public , noninvertible *)

(* 2. Adversary capabilities and functions *)

(* Encryption scheme -- ElGamal/ElGamal with multiple encryptions -- Key pairs generated through Gen_e are implicit. *)
type private_ekey. fun t_private_ekey(private_ekey): bitstring [data,typeConverter]. (* The type for private decryption keys + type converter. *)
type public_ekey. fun t_public_ekey(public_ekey) : bitstring [data,typeConverter]. (* The type for public encryption keys + type converter. *)
fun ske(agent_id) : private_ekey [private]. (* The private key associated to an agent_id # private *)
fun pube(private_ekey) : public_ekey. (* The function to rebuild a public key from the private # public , noninvertible *)
letfun pke(Id:agent_id) = pube(ske(Id)). (* The public encryption key associated to an agent id # public , invertible *)
free kCCR1 : private_ekey [private]. (* Secret key for choice return codes' KDF computations of in the honest CCR *)
free kCCR2: private_ekey. (* Secret key choice return codes'for KDF computations in the dishonest CCR *)
free kCCR1' : private_ekey [private]. (* Secret key for vote cast return codes' KDF computations in the honest CCR *)
free kCCR2': private_ekey. (* Secret key for vote cast return codes' KDF computations in the dishonest CCR *)
free setupSecretKey: private_ekey [private]. (* Secret key used by the Setup Component during the setup process *)
free ELSk1:private_ekey. (* Election decryption key share held by the (dishonest) CCM1 *)
free ELSk2: private_ekey. (* Election decryption key share held by the dishonest CCM2 *)

fun Enc_c1( num) : bitstring. (* The c1 part of the asymmetric encryption function with explicit random number. *)
fun Enc_c2(public_ekey,bitstring,num) : bitstring. (* The c2 part of the asymmetric encryption function with explicit random number. *)
letfun Enc(Pk:public_ekey,M:bitstring,R:num) = (Enc_c1(R),Enc_c2(Pk,M,R)).
reduc forall Sk:private_ekey, M:bitstring, R:num; Dec(Sk,(Enc_c1(R),Enc_c2(pube(Sk),M,R))) = M. (* Decryption *)
reduc forall Pk:public_ekey, M:bitstring, R:num; VerifE(Pk,(Enc_c1(R),Enc_c2(Pk,M,R))) = true. (* Checks key *)
reduc forall Id:agent_id; Get_Id(pube(ske(Id))) = Id. (* Extract Id - even if this computation is not possible with ElGamal encryptions, it strengthens the attacker capabilities in the model *)

(* Signature used to model messages exchanged between the Setup Component and the CCRs  - see the section on channel security in the system specification *)
type private_skey. fun t_private_skey(private_skey): bitstring [data,typeConverter]. (* The type for private signature keys + type converter. *)
type public_skey. fun t_public_skey(public_skey) : bitstring [data,typeConverter]. (* The type for public signature keys + type converter. *)
fun pubs(private_skey) : public_skey. (* The function to rebuild a public key from the private # public , noninvertible *)
free ABsk : private_skey [private]. (* The private signature key used by the Setup Component - ABsk refers to the administration board secret key *)
fun sign(private_skey,bitstring) : bitstring.
reduc forall Sk:private_skey, M:bitstring; checkSign(pubs(Sk),sign(Sk,M)) = M.

(* Modeling of the voting phase non-interactive zero-knowledge Proofs. *)
(* 'Exponentiation Proof' and 'Plaintext Equality Proof' are abstracted inside the *)
(* ZKP and VerifP operators. Consequently, the c1~ and c2~, which are intermediate values, are not needed anymore and thus abstracted away. *)
fun pCC(private_ekey,bitstring) : bitstring. (* The function to generate partial Choice Return Codes (kID,vi) = vi^kId # public , noninvertible *)
fun tild(private_ekey,bitstring): bitstring. (* To exponentiate ciphertexts generated by the function Enc # public , noninvertible *)
(* Note that pCC() and tild() both model an exponentiation. Two symbols are defined for readability purposes. *)

fun ZKP(public_ekey, public_ekey, bitstring, bitstring, num, private_ekey) : bitstring. (* Modeling of the proof generation *)
reduc forall ELpk:public_ekey, KVCId: public_ekey, J1:num,J2:num, R:num, vcid: agent_id;
    VerifP(ELpk, pube(ske(vcid)), vcid, (Enc_c1(R),Enc_c2(ELpk,(v(J1),v(J2)),R)),(pCC(ske(vcid), v(J1)),pCC(ske(vcid), v(J2))), ZKP(ELpk, pube(ske(vcid)), (Enc_c1(R),Enc_c2(ELpk,(v(J1),v(J2)),R)), (pCC(ske(vcid),v(J1)),pCC(ske(vcid),v(J2))), R, ske(vcid))) = true.

(* Next follows a series of functions that model modular multiplications that take place at several parts of the protocol *)
(* Different functions names imply they are applied to different input Types *)
fun mergepk(public_ekey,public_ekey): public_ekey.
fun mergeExpo(bitstring,bitstring):bitstring.

reduc forall sk1: private_ekey, sk2:private_ekey, M: bitstring, R:num; DecMerge(sk1, sk2, (Enc_c1(R), Enc_c2(mergepk(pube(sk1), pube(sk2)), M, R))) = M.

reduc forall sk:private_ekey, kVCId:private_ekey, kId1:private_ekey, kId2:private_ekey, R1:num,R2:num,R3:num,R4:num, M:bitstring, J1:num,J2:num,J3:num,J4:num;
(* rule to compute pCId_i's in algorithm GenCMTable of the protocol SetupVoting *)
(* Note that the rule combines the two functions CombineEncLongCodeShares and the decryption algorithm *)
DecPCC(sk,
    mergeExpo(tild(kId1,
    ((Enc_c1(R1), Enc_c2(pube(sk), H1(pCC(kVCId, v(J1))), R1)),(Enc_c1(R2), Enc_c2(pube(sk), H1(pCC(kVCId, v(J2))), R2)),(Enc_c1(R3), Enc_c2(pube(sk), H1(pCC(kVCId, v(J3))), R3)),(Enc_c1(R4), Enc_c2(pube(sk), H1(pCC(kVCId, v(J4))), R4)))),
    tild(kId2,
    ((Enc_c1(R1), Enc_c2(pube(sk), H1(pCC(kVCId, v(J1))), R1)),(Enc_c1(R2), Enc_c2(pube(sk), H1(pCC(kVCId, v(J2))), R2)),(Enc_c1(R3), Enc_c2(pube(sk), H1(pCC(kVCId, v(J3))), R3)),(Enc_c1(R4), Enc_c2(pube(sk), H1(pCC(kVCId, v(J4))), R4))))
    )) =
    (mergeExpo(tild(kId1, H1(pCC(kVCId, v(J1)))), tild(kId2, H1(pCC(kVCId, v(J1))))),mergeExpo(tild(kId1, H1(pCC(kVCId, v(J2)))), tild(kId2, H1(pCC(kVCId, v(J2))))),mergeExpo(tild(kId1, H1(pCC(kVCId, v(J3)))), tild(kId2, H1(pCC(kVCId, v(J3))))),mergeExpo(tild(kId1, H1(pCC(kVCId, v(J4)))), tild(kId2, H1(pCC(kVCId, v(J4)))))).

reduc forall sk:private_ekey, kVCId:private_ekey, kId1:private_ekey, kId2:private_ekey, R:num, M:bitstring;
(* states that pVCCId coincides in algorithms GenCMTable (of protocol SetupVoting) and algorithm ExtractVCC (of protocol ConfirmVote) *)
(* even if this equation is similar to the previous one, it does not involve the 'pCC(.)' symbol to exponentiate the message M;
it uses instead the symbol 'tild(.)' for readability purposes w.r.t. the specification *)
DecPCCOneHash(sk,
    mergeExpo(tild(kId1,
    (Enc_c1(R), Enc_c2(pube(sk), H1( tild(kVCId,M)), R))),
    tild(kId2,
    (Enc_c1(R), Enc_c2(pube(sk), H1( tild(kVCId,M)), R))))
    ) =
mergeExpo(tild(kId1, H1(tild(kVCId, M))),
    tild(kId2, H1(tild(kVCId, M)))).

(* Symmetric encryption scheme -- AES-GCM *)
type symmetric_ekey. fun t_symmetric_ekey(symmetric_ekey): bitstring [data,typeConverter]. (* The type for symmetric encryption keys. *)
fun Enc_s(symmetric_ekey,bitstring) : bitstring. (* Encryption (symmetric key) *)
reduc forall SKey:symmetric_ekey, M:bitstring; Dec_s(SKey,Enc_s(SKey,M)) = M. (* Decryption (symmetric key) *)


(* Key and IDs derivation scheme *)
fun deltaId(agent_id) : agent_id. (* The delta function, for agents IDs # public *)
fun deltaKey(password) : symmetric_ekey. (* The delta function, for symmetric encryption keys for keystore # public *)
fun deltaPassword(agent_id): password [private]. (* The delta function to assign passwords (the start voting key SVK) to voters # private *)
fun deltaKeyAgent(private_ekey,agent_id) : private_ekey. (* Key Derivation Function (KDF) used by the CCRs to create kId1, kId2,.... # public *)
fun delta(bitstring) : symmetric_ekey. (* The mask generation function (see the algorithm "KeyDerivationFunction" in the system specification ) # public , noninvertible *)
fun honest(agent_id) : agent_id [private]. (* MODELING : a function to separate honest and dishonest agents (only in Properties) *)

fun rand(bitstring,bitstring):num. (* to sample random nonces when Setup Component encrypts voting options in the Setup phase *)
fun rand2num(bitstring,agent_id): num. (* rand2 and rand2num are used to sample random nonces that are unique per agent_id *)

(* 3. Initialization sequence (done offline in this modeling). *)
fun bck(agent_id) : bitstring [private]. (* The Ballot Casting Key 'BCK^id' of an agent id # private *)
fun CC(agent_id,bitstring) : bitstring [private]. (* The short choice return code 'CC_{id,k}' of an agent id + voting option k # private *)
fun VCC(agent_id) : bitstring [private]. (* The short vote cast return code 'VCC_id' of an agent id # private *)
fun CCM_table(bitstring) : bitstring [private]. (* The Codes Mapping Table contained CC codes -- written only by the Setup Component (private) but opened by everyone using readCC(). *)
fun VCM_table(bitstring) : bitstring [private]. (* The Codes Mapping Table contained VCC codes -- written only by the Setup Component (private) but opened by everyone using readVCC(). *)
reduc forall VCid:agent_id, J:num, pC:bitstring ;
    readCC( H1(H1((VCid, pC))) , CCM_table(Enc_s( delta( H1((VCid, pC)) ) , CC(VCid,v(J))))) =
      Enc_s( delta( H1((VCid, pC)) ) , CC(VCid,v(J)) ).
reduc forall VCid:agent_id, CMId:bitstring, vCCId: bitstring; (* Enc_s(VCC,VCC) *)
    readVCC( H1(H1((VCid, CMId))) , VCM_table(Enc_s( delta( H1 ((VCid, CMId)) ) , (vCCId, VCC( VCid))))) =
      Enc_s( delta( H1((VCid, CMId)) ), (vCCId, VCC(VCid))).

(* Registration data for any voter id -- AliceData(svk) produced by the Setup Component for Alice -- opened with a reduction *)
fun AliceData(agent_id) : bitstring [private].
reduc forall id_Alice:agent_id, J1:num,J2:num;
    GetAliceData(AliceData(id_Alice),J1,J2) = (id_Alice, deltaId(id_Alice), deltaPassword(id_Alice), bck(id_Alice), VCC(deltaId(id_Alice)), CC(deltaId(id_Alice),v(J1)),CC(deltaId(id_Alice),v(J2))).
(* Note: The code CC(id,v(J)) is the Choice Return Code of the intended vote of Alice. *)

(* Registration data for the Voting Server -- ServData(pke,sks) produced by the Setup Component -- opened with a reduction *)
fun ServData(public_ekey) : bitstring [private].
reduc forall ELpk:public_ekey, id_Voter:agent_id;
    GetServData(ServData(ELpk),id_Voter) = (ELpk, Enc_s(deltaKey(deltaPassword(id_Voter)),t_private_ekey(ske((id_Voter))))).

(* 4. Algebraic properties and List of Events *)
event HappyUser(bitstring, num,num). (* Issued by the voter when he/she terminates successfully. *)
event SetupComponentCompleted(agent_id).
event InsertBBCCR( agent_id, bitstring). (* Issued by the CCR when it adds something into the ballot box (BB). *)
event BallotConfirmationProcessed(agent_id, bitstring). (* Issued by the CCR when it confirms a ballot in the ballot box (BB). *)

(* 5. Methods and Agents processes *)

(* CreateVote(ELpk,VCid,Vopt,VCidpk,VCidsk,pkCCRs,R,R'). Voting Client creates the ballot. Algorithm CreateVote in protocol SendVote.
In the symbolic model, the voting client sends the unencrypted partial choice return codes (E2) to the voting server. *)
(* NOTE- This function is not used in the model since the voting client is dishonest and able to forge the ballot. However, we keep it for the sake of readability. Indeed, it defines what is a ballot in this ProVerif model. *)
letfun CreateVote(ELpk:public_ekey,VCid:agent_id,J1:num,KVCId:public_ekey,kVCId:private_ekey) =
    let V = (v(J1)) in new R:num;
    let E1 = Enc(ELpk, V, R) in
    let E2 = (pCC(kVCId,v(J1))) in
    let P = ZKP(ELpk,KVCId,E1,E2, R, kVCId) in
    (E1,E2, tild(kVCId,E1), KVCId, P).

(* ProcessVoteCheck(ELpk,VCid,B,pkCCRs) -- corresponds to the algorithm VerifyBallotCCR of the protocol SendVote *)
letfun ProcessVoteCheck(ELpk:public_ekey,VCid:agent_id,B:bitstring) =
    let (xE1:bitstring, xE2:bitstring, xKVCId: public_ekey, xP:bitstring) = B in
    let Ok1 = VerifP(ELpk,xKVCId, VCid, xE1, xE2, xP) in
    true.

(* Typing of the messages *)
(* These are public tags that are added on top of messages, to indicate from which step they originate. *)
(* They do not affect the executability nor the semantics of the protocol since they are
public, i.e. known by the attacker who can also change them. *)
free mAC1, mAC2, mCA1, mCA2 : bitstring.
free ElementMapping, KeyMapping, EncVotingOptions, ExpoVotingOptions, EncBCK, ExpoBCK, VCIdTag, CMTag, VCCidTag, vCCIdTag, E2ExpoTag: bitstring.

(* Alice -- The client process *)
let Alice(Ch1:channel,InitData:bitstring,J1:num,J2:num) =
    (* Checks that all voting options are different *)
    if J2 = J1 then 0 else (* No honest voter can use twice the same option *)
    (* Retrieves registration data obtained from the Setup Component -- Set of initial data given to Alice by the Setup Component. *)
    let (id_Alice: agent_id, VCid: agent_id, SVKid:password, BCKid:bitstring, VCCid:bitstring, CCid1:bitstring,CCid2:bitstring) = GetAliceData(InitData,J1,J2) in
    out(AliceSCChannel, id_Alice);

    (* Voting part -- The voting process followed by agent Alice *)
    out(Ch1, ( mAC1,VCid, SVKid,J1,J2));
    in( Ch1, (=mCA1,(CC_Received1:bitstring,CC_Received2:bitstring)));
    if ((CC_Received1=CCid1) || (CC_Received2=CCid1)) && ((CC_Received1=CCid2) || (CC_Received2=CCid2)) then (* Compares the short Choice Return Codes. *)
    (
    out(Ch1, ( mAC2,BCKid));
    in( Ch1, (=mCA2,=VCCid)); (* Alice checks the Vote Cast Return Code *)
    event HappyUser(InitData, J1,J2)
    ).


(* `CCRExpoVotingOptions` and `CCRExpoBCK` correspond to algorithm GenEncLongCodeShares of the protocol SetupVoting *)
(* They allow an honest CCR to compute the exponentiation of the received message, to his key. *)
(* It is important to note that the KDF is partially abstracted too.
A CCR is created with two private keys: kCCR1 (resp. kCCR2) to exponentiate ctxtvId (=c_pCC) and
kCCR1' (resp. kCCR2') to exponentiate ctxtbId (=c_ck) *)
(* More precisely: deltaKeyAgent(kCCRi,VCId) = DeriveKey(k'_i, vcid) and
deltaKeyAgent(kCCRi',VCId) = DeriveKey(k'_i, vcid || "confirm") in algorithm GenEncLongCodeShares in the system specification. *)
(* Finally, this function is executed by the Setup Component process, which abstracts
the communications between the Setup Component and the honest CCR (the proofs are thus omitted).
However, even if the model assumes that the attacker cannot interfere on this channel,
the messages that are exchanged (i.e. ctctvId/ctxtbId and the outputs of the functions
are revealed to the attacker to model a read-only channel. *)
letfun CCRExpoVotingOptions(ctxtvId:bitstring, VCId:agent_id) =
    (tild(deltaKeyAgent(kCCR1,VCId), ctxtvId)).

letfun CCRExpoBCK(ctxtbId:bitstring,VCId:agent_id) =
    (tild(deltaKeyAgent(kCCR1',VCId), ctxtbId)).

(* The Choice Return Codes Control Components - CCR_1, CCR_2 - one of which is untrustworthy *)
let CCR(i:num,k:private_ekey, k':private_ekey, SC_CCRChannel:channel, ABpk:public_skey) =
    (** setup phase - setupCCR **)

    (* generate the private/public key pairs *)
    in(SC_CCRChannel, (=VCIdTag, vcid: agent_id));

    (* Compute k_{j,id}/K_{j,id} w.r.t. to the control components' keys in the system specification *)
    let kId = deltaKeyAgent(k,vcid) in
    out(c, pube(kId)) ;

    (* Compute k'_{j,id}/K'_{j,id} w.r.t. to the control components' keys in the system specification  *)
    let kId' = deltaKeyAgent(k',vcid) in
    out(c, pube(kId')) ;

    (* createCC *)
    (* exponentiate E2 *)

    let ELpk = mergepk(pube(ELSk1), pube(ELSk2)) in

    (* message of the setup process; others are abstracted in the Setup Component process *)
    in(c, lpCCinit:bitstring);
    let (LpCC1:bitstring,LpCC2:bitstring,LpCC3:bitstring,LpCC4:bitstring) = checkSign(ABpk,lpCCinit) in

    (** voting phase **)

    (* The CCR receives a ballot *)
    in(c, B:bitstring);
    let (E1:bitstring,E2:bitstring, KVCId:public_ekey, P:bitstring) = B in

    if ProcessVoteCheck(ELpk, vcid, B) = true then
      (* the PartialDecryptPCCj and DecryptPCCj are not described since E2 is sent as a plaintext *)
      let (pCC1:bitstring,pCC2:bitstring) = E2 in (* E2 is not encrypted and constrains the pCC as a tuple *)
      let (hpCC1:bitstring,hpCC2:bitstring) = (H1(pCC1), H1(pCC2)) in
      let (lpCC1:bitstring, lpCC2:bitstring) = (H1((vcid,hpCC1)), H1((vcid,hpCC2))) in

      if (lpCC1 = LpCC1 || lpCC1 = LpCC2 || lpCC1 = LpCC3 || lpCC1 = LpCC4)
         && (lpCC2 = LpCC1 || lpCC2 = LpCC2 || lpCC2 = LpCC3 || lpCC2 = LpCC4) then (

        event InsertBBCCR(vcid, B); (* The honest CCR stores in its internal log that it has proceeded the pair (vcid,B) *)
        out(c, (E2ExpoTag, i, (tild(kId,H1(pCC1)),tild(kId,H1(pCC2))))); (* output of lCCj *)

        (* Process Confirm *)
        in(c, (=CMTag, pCM:bitstring));
        event BallotConfirmationProcessed(vcid, B); (* The honest CCR stores in its internal log that it has contributed to the confirmation of the pair (vcid,B) *)
        out(c, tild(kId', H1(pCM)))
      ) else 0
    .

(* In order to model that the CCR accepts at most one ballot per voter, we consider the following restriction. Only traces satisfying this restriction will be considered by ProVerif. *)
restriction vcid:agent_id, B1,B2:bitstring; event(InsertBBCCR(vcid,B1)) && event(InsertBBCCR(vcid,B2)) ==> B1 = B2.

(* Setup Component - trustworthy *)
let SetupComponent(SC_CCRChannel:channel, J1:num,J2:num,J3:num,J4:num) =
    (* the Setup Component reveals the public part of its signature key, i.e. ABpk *)
    out(c, pubs(ABsk));

    (* setup phase *)
    new r: bitstring;
    new r': bitstring;
    new R2:bitstring;

    in(UserChannel, id_Voter:agent_id); (* Setup Component receives the voter's pseudonym id *)
    let VCId:agent_id = deltaId(id_Voter) in
    out(SC_CCRChannel, (VCIdTag, VCId));
    let svk:password = deltaPassword(VCId) in

    (* generate the keys *)
    let kVCId: private_ekey = ske(VCId) in
    out(c, pube(kVCId));
    (* encrypt set of voting options *)
    let (r1:num,r2:num,r3:num,r4:num) = (rand(r, v(J1)),rand(r, v(J2)),rand(r, v(J3)),rand(r, v(J4))) in
    let hpCC1 = H1( pCC(kVCId,v(J1))) in
    let hpCC2 = H1( pCC(kVCId,v(J2))) in
    let hpCC3 = H1( pCC(kVCId,v(J3))) in
    let hpCC4 = H1( pCC(kVCId,v(J4))) in
    let ctxtvID:bitstring = (Enc(pube(setupSecretKey), hpCC1, r1) ,Enc(pube(setupSecretKey), hpCC2, r2) ,Enc(pube(setupSecretKey), hpCC3, r3) ,Enc(pube(setupSecretKey), hpCC4, r4) ) in (* ctxtvID = c_{pCC,id} in algorithm GenVerDat in the system specification *)

    (* compute the encrypted ballot casting key (BCK) *)
    let BCKId = bck(id_Voter) in
    let R = rand2num(r', id_Voter) in
    let ctxtbId = Enc(pube(setupSecretKey), H1( tild(kVCId,BCKId) ) , R) in (* ctxtbId = c_{ck,id} in algorithm GenVerDat in the system specification *)
    let lpCC1 = H1((VCId, hpCC1)) in
    let lpCC2 = H1((VCId, hpCC2)) in
    let lpCC3 = H1((VCId, hpCC3)) in
    let lpCC4 = H1((VCId, hpCC4)) in

    out(c, (EncVotingOptions, ctxtvID));
    out(c, (EncBCK, ctxtbId));
    out(c, sign(ABsk, (lpCC1,lpCC2,lpCC3,lpCC4))); (* for this communication, we model the signature used by the Setup Component when sending messages to the CCRs *)

    (* wait for the exponentiations from all CCRs *)
    let (ctxtvIDExpo1) = CCRExpoVotingOptions(ctxtvID,VCId) in (* abstraction of the 'input' of the exponentiation computed by CCR1 (honest) *)
    out(c, ctxtvIDExpo1); (* reveals that this message should have been sent by CCR1 and thus may be known by the attacker spying on the channel *)
    in(c, (=ExpoVotingOptions, =CCRId2, ctxtvIDExpo2:bitstring)); (* input of the exponentiation computed by CCR2 (dishonest) *)

    (* Combine exponentiated ciphertexts of the long Choice Return Codes shares received from the CCRs and decrypt the result. Corresponds to algorithms CombineEncLongCodeShares and GenCMTable of the protocol SetupVoting *)
    let ctxtpcId: bitstring = mergeExpo(ctxtvIDExpo1, ctxtvIDExpo2) in (* ctxtpcId = c_{pC,id} in algorithm GenCMTable in the system specification *)

    (* Combine exponentiated ciphertexts of the long Vote Cast Return Code shares received from CCRs. Corresponds to algorithm CombineEncLongCodeShares of the protocol SetupVoting *)
    let (tildctxbID_1) = CCRExpoBCK(ctxtbId,VCId) in (* abstraction of the 'input' of the exponentiation computed by CCR1 (honest) *)
    out(c, tildctxbID_1); (* reveals that this message should have been sent by CCR1 and thus may be known by the attacker spying on the channel *)
    in(c, (=ExpoBCK, =CCRId2,tildctxbID_2:bitstring)); (* input of the exponentiation computed by CCR2 (dishonest) *)

    let ctxbckId = mergeExpo(tildctxbID_1, tildctxbID_2) in (* ctxbckId = c_{pVCC,id} in algorithm GenCMTable in the system specification *)

    (* Main steps of algorithm GenCMTable in the system specification *)
    let pCId = DecPCC(setupSecretKey, ctxtpcId) in
    let (pCId1:bitstring,pCId2:bitstring,pCId3:bitstring,pCId4:bitstring) = pCId in
    let (lCCId1:symmetric_ekey,lCCId2:symmetric_ekey,lCCId3:symmetric_ekey,lCCId4:symmetric_ekey) = (delta( H1((VCId, pCId1)) ) ,delta( H1((VCId, pCId2)) ) ,delta( H1((VCId, pCId3)) ) ,delta( H1((VCId, pCId4)) ) ) in
    let CCMtable = (CCM_table(Enc_s(lCCId1, CC(VCId,v(J1)))) ,CCM_table(Enc_s(lCCId2, CC(VCId,v(J2)))) ,CCM_table(Enc_s(lCCId3, CC(VCId,v(J3)))) ,CCM_table(Enc_s(lCCId4, CC(VCId,v(J4)))) ) in

    let CMId = DecPCCOneHash(setupSecretKey, ctxbckId) in
    let lvCCId:symmetric_ekey = delta( H1( (VCId, CMId)) ) in
    let vCCId = VCC(VCId) in
    let VCMtable = VCM_table(Enc_s(lvCCId, (vCCId, VCC(VCId)))) in

    (* output of the table *)
    let CCkeys = ( H1(H1((VCId, pCId1))), H1(H1((VCId, pCId2))), H1(H1((VCId, pCId3))), H1(H1((VCId, pCId4))) ) in
    let VCCkey = H1(H1( (VCId, CMId))) in
    out(c, (KeyMapping, (CCkeys, VCCkey)));
    out (c, (ElementMapping, (CCMtable, VCMtable)));
    event SetupComponentCompleted(VCId);
    0.


(* 6. Second Phase -- The Tally after all votes are collected. *)

(* No Tally for individual verifiability properties, only for universal verifiability. *)

(* 7. Security properties *)

(* Sanity check *)
(* ProVerif must not return 'true' i.e. must return 'false' or at least 'cannot be proved' (because over-approximations) *)
query vcid:agent_id; event(SetupComponentCompleted(vcid)).
query id_alice:agent_id; event(HappyUser(AliceData(id_alice), ja1, ja2)).

(* Individual Verifiability Property : if the honest voter is happy then a ballot corresponding to the voter's intention must have been inserted and confirmed in the ballot-box *)
(* Consequently, if the voting choices do not match the voter's intentions, then the voting server cheated. *)
query Id:agent_id, ELpk:public_ekey,
    J1:num,J2:num,
    j1:num, j2:num, E21:bitstring, R1:num, P1:bitstring,
    VCid:agent_id;
    event(HappyUser(AliceData(honest(Id)), J1,J2)) ==>
    VCid = deltaId(honest(Id))
    && event(InsertBBCCR(VCid, ((Enc_c1(R1),Enc_c2(ELpk,(v(j1),v(j2)),R1)),E21,pube(ske(VCid)),P1)))
    && event(BallotConfirmationProcessed(VCid, ((Enc_c1(R1),Enc_c2(ELpk,(v(j1),v(j2)),R1)),E21,pube(ske(VCid)),P1)))
      && ((j1 = J1 && j2 = J2) || (j1 = J2 && j2 = J1)).


(* 8. Main process -- initiates the election *)

process
(* Public output from Setup(..) -- Gives the election's parameters to the adversary. *)
out(c, (pube(ELSk1),pube(ELSk2))); (* public CCMs' encryption keys *)
let ELpk = mergepk(pube(ELSk1), pube(ELSk2)) in out(c, ELpk); (* public election key *)
out(c, pube(setupSecretKey)); (* public Setup Component's encryption key *)
out (c, (pube(kCCR1),pube(kCCR2))); (* public CCRs' keys *)

(* The voting server is dishonest *)
out(c, ServData(ELpk));

(* Dishonest voter(s) : As many as possible for verifiability. *)
!(new id:agent_id; out(c,id); out(c,AliceData(id))) |

(* Gives the honest voter's public data to the adversary *)
out(c, deltaId(honest(idA))) |

(* Roles for honest voter(s) -- one for Individual Verifiability. *)
Alice( Ch1, AliceData(honest(idA)),ja1,ja2 ) |
out(c, ske(deltaId(honest(idA)))) (* the voting device is corrupted *)

(* The Setup Component is honest *)
| !SetupComponent(SC_CCRChannel, ja1,ja2,ja3,ja4)

(* CCR1 is honest *)
| !CCR(CCRId1, kCCR1, kCCR1', SC_CCRChannel, pubs(ABsk))
(* note that the material of CCR2 (dishonest) is public, i.e. kCCR2 and kCCR2' are already known by the attacker *)
