# Changelog

## Release 0.8.5.5 (2022-06-24)

### Updated (11)

- updated [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting#change-log-release-015)
- updated [Swiss Post Voting Protocol](Protocol/Swiss_Post_Voting_Protocol_Computational_proof.pdf)
- updated [Architecture of the Swiss Post Voting System](System/SwissPost_Voting_System_architecture_document.pdf)
- updated [System specification](/System)
- updated [crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#change-log-release-015)
- updated [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain#change-log-release-015)
- updated [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts#change-log-release-015)
- updated [Product roadmap](/Product/Product_Roadmap.md)
- updated [Verifier specification](https://gitlab.com/swisspost-evoting/System/Verifier_Specification.pdf)
- updated [Trusted Build of the Swiss Post Voting System](/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md)

### Fixed (5)
- minor corrections [Recommendation safety measures (cantonal infrastructure)](/Operations/Recommendation_Safety_Measures_Cantonal_Infrastructure.md)
- minor corrections [Operation whitepaper of the Swiss Post voting system](/Operations/Operation%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- minor corrections [Software development process of the Swiss Post voting system](/Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- minor corrections [ModSec & CRS Tuning process](Operations/ModSecurity-CRS-Tuning-Concept.md)
- minor corrections [Test concept of the swiss post voting system](Testing/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md)

---

## Release 0.8.5.4 (2022-04-21)

### Updated (8)

- updated [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting#change-log-release-014)
- updated [System specification](/System#changes-in-version-099)
- updated [crypto-primitives-specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
- updated [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#change-log-release-014)
- updated [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain#change-log-release-014)
- updated [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts#change-log-release-014)
- updated [Product roadmap](/Product/Product_Roadmap.md)
- updated [Readme of Swiss Post's reponses to Examination 2021](/Reports/Examination2021/README.md)

### Fixed (1)
- correction to the [evoting-e2e-dev](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev)

---

## Release 0.8.5.3 (2022-04-20)

### Added (1)

- [Swiss Post’s reports in response to the expert reports](/Reports/Examination2021)

---

## Release 0.8.5.2 (2022-02-18)

### Added (2)

- New repository [crypto-primitives-ts](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts) for the typescript of the e-voting-client
- [Security advices](/Security-advices): Tips and instructions that voters can carry out on the device used for voting to ensure their vote has not been manipulated

### Updated (8)

- updated [e-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting)
- updated [system specification](/System#changes-in-version-098)
- updated [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#change-log-release-013)
- updated [crypto-primitives specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#change-log-release-013)
- updated [crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain#change-log-release-013)
- updated [verifier](https://gitlab.com/swisspost-evoting/verifier/verifier)
- updated [Product roadmap](/Product/Product_Roadmap.md)
- images in [Trusted Build of the Swiss Post Voting System](/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md) and [Infrastructure whitepaper of the Swiss Post voting system](/Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)

### Fixed (4)

- fixed smaller typos and text in [Trusted Build of the Swiss Post Voting System](/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md)
- fixed smaller typos and text in [Infrastructure whitepaper of the Swiss Post voting system](/Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- fixed broken urls in [Software development process of the Swiss Post voting system](/Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- correction to the [E2E(evoting-e2e-dev)environment](https://gitlab.com/swisspost-evoting/e-voting/evoting-e2e-dev)

---

## Release 0.8.5.1 (2022-02-04)

### Added (1)

- [The challenges of enabling public scrutiny, E-Vote-ID 2021](/Reports/e_vote_2021.pdf) under /Reports

### Fixed (2)

- fixed whitespaces in .gitlab-ci.yml
- fixed smaller typos and errors

---

## Release 0.8.5.0 (2021-11-15)

### Updated (3)

- [README](README.md)
- [E-voting sourcecode](https://gitlab.com/swisspost-evoting/e-voting/e-voting)
- [Crypto-primitives sourcecode](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives)

### Added (5)

- Gitlab-script based on pandocker with custom Eisvogel-template to convert markdown documents into pdf documents
- [Trusted Build of the Swiss Post Voting System](/Operations/Trusted%20Build%20of%20the%20Swiss%20Post%20Voting%20System.md)
- [Operation whitepaper of the Swiss Post voting system](/Operations/Operation%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- [Crypto-primitives-domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain)
- [Verifier sourcecode](https://gitlab.com/swisspost-evoting/verifier/verifier)

### Fixed (1)

- fixed smaller typos and errors

---

## Release 0.8.4.0 (2021-10-15)

### Updated (3)

- [Protocol](/Protocol)
- [System Specification](/System/System_Specification.pdf)
- [Verifier Specification](https://gitlab.com/swisspost-evoting/verifier/verifier)

---

## Release 0.8.3.0 (2021-09-01)

### Added (7)

- [Symbolic proofs](Symbolic-models)
- [ModSec & CRS Tuning process](Operations/ModSecurity-CRS-Tuning-Concept.md)
- [Verifier specification](https://gitlab.com/swisspost-evoting/verifier)
- [SDM Hardening guidlines](Operations/Recommendation_Safety_Measures_SDM.md)
- [ABOUT](ABOUT.md)
- [Auditability report](Reports/Evoting-Auditability-August-2021.pdf)
- [E-voting sourcecode](https://gitlab.com/swisspost-evoting/e-voting/e-voting)

### Fixed (4)

- fixed smaller typos and errors
- improved project structure and wording
- improved README
- improved markdown compliance according to markdownlint

---

## Release 0.8.2.0 (2021-07-19)

### Added (4 Changes)

- [Software development process of the Swiss Post voting system](/Product/Software%20development%20process%20of%20the%20Swiss%20Post%20voting%20system.md)
- [Infrastructure whitepaper of the Swiss Post voting system](/Operations/Infrastructure%20whitepaper%20of%20the%20Swiss%20Post%20voting%20system.md)
- [E-voting Architecture documentation of the Swiss Post Voting System](/System/SwissPost_Voting_System_architecture_document.pdf)
- [Test Concept of the Swiss Post Voting System](/Testing/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md)

---

## Release 0.8.1.0 (2021-06-25)

### Fixed (6 Changes)

- minor structural and wording improvements
- fixed smaller typos and errors
- markdown compliance according to markdownlint
- new version of the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
- new version of the [protocol](/Protocol)
- new version of the [system specification](/System/System_Specification.pdf)

---

## Release 0.8.0.0 (2021-05-04)

### Added (5 Changes)

- new directory [System](/System) in /Documentation
  - [Readme](/System/README.md) as accompanying document for the System specifications
    - [System specification](/System/System_Specification.pdf)
- new file [ElectoralModel](/Product/ElectoralModel.md)
- added a changelog-section for the crypto-primitives in the [crypto-primitives readme](https://gitlab.com/swisspost-evoting/crypto-primitives/-/blob/master/README.md)

### Fixed (5 Changes)

- structural and wording improvement for the [/Documentation/README.md](/README.md)
- structural and wording improvement for the [overviewfile](Product/Overview.md)
- structural and wording improvement for the [reportingfile](/REPORTING.md)
- new version of the [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
- new version of the [protocol](/Protocol)

---

## Release 0.7.0.0 (2021-03-23)

### Added (7 changes)

- new repository [crypto-primitives](https://gitlab.com/swisspost-evoting/crypto-primitives)
  - [README.md](https://gitlab.com/swisspost-evoting/crypto-primitives/readme.md) as accompanying document for the crypto-primitives
    - [Specification](https://gitlab.com/swisspost-evoting/crypto-primitives/-/blob/master/cryptographic_primitives_specification.pdf) of the crypto-primitives
- new directory [src](https://gitlab.com/swisspost-evoting/crypto-primitives/src) in /crypto-primitives
  - [Crypto-primitives sourcecode](https://gitlab.com/swisspost-evoting/crypto-primitives/src)
- new directory [product](Product)
  - new File [overview](Product/Overview.md)

### Fixed (4 changes)

- renamed 'Protocol definition' to [/Documentation/Protocol](Protocol) for user-friendliness
- wording improvement for the [/Documentation/Protocol/README.md](/Protocol/README.md)
- Changelog-section in README of [/Documentation](/README.md)
- Overview-section in README of [/Documentation](/README.md)