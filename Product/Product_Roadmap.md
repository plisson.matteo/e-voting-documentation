# Product Roadmap

The roadmap sets out the dates for the availability of releases and documents that are planned to be made available. The planning is currently made up to the production release and will be extended later.

In addition, the information on GitLab is always up to date:

- [Overall Changelog](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/CHANGELOG.md)
- [Readme E-voting](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/blob/master/README.md)
- [Readme Crypto-Primitives](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/README.md)
- [Readme Crypto-Primitives Typescript](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-ts/-/blob/master/README.md)
- [Readme Crypto-Primitives Domain](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives-domain/-/blob/master/README.md)
- [Readme Verifier](https://gitlab.com/swisspost-evoting/verifier/verifier/-/blob/master/README.md)


## Upcoming Releases 2022
- Address findings from the ongoing independent examination [mandated by the Federal Chancellery](https://www.bk.admin.ch/bk/en/home/dokumentation/medienmitteilungen.msg-id-84337.html), as set out in the plan of action.
- The following artefacts will be updated:

#### Symbolic Analysis, Q3 2022
- Address the changes done to the protocol related to <https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/11> in the [Symbolic Analysis](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Symbolic-models).

#### Protocol of the Swiss Post Voting System, Q3 2022
- Update of Part III and IV of the [Computational Proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol).

#### Verifier, Q3 2022
- Alignment of the [Verifier source code](https://gitlab.com/swisspost-evoting/verifier/verifier).

#### E-voting System, Q4 2022
Address the following issues from <https://gitlab.com/swisspost-evoting/e-voting/e-voting#known-issues>
 - Voter Portal rewrite in Angular
 - Write-Ins 
 - Performance improvements for extended security level



## Future Releases (after Relaunch)

- Address further known issues and future work 
- Further findings of lower priority from the ongoing independent examination [mandated by the Federal Chancellery](https://www.bk.admin.ch/bk/en/home/dokumentation/medienmitteilungen.msg-id-84337.html) will be addressed in future improvements, in line with already known [measures communicated from the Federal Chancellery](https://www.bk.admin.ch/dam/bk/en/dokumente/pore/Final%20report%20SC%20VE_November%202020.pdf.download.pdf/Final%20report%20SC%20VE_November%202020.pdf).

