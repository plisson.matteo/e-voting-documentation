# What is the content of this folder?

| Document | Content | Download |
| :------- |:--------|:---------|
| [Test Concept of the Swiss Post Voting System](Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.md) | This document describes the test concept for the e-voting service. The test concept shows the test procedure, defines the cooperation and the responsibilities of all persons involved in the test process. | <a href="https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/jobs/artifacts/master/raw/Test%20Concept%20of%20the%20Swiss%20Post%20Voting%20System.pdf?job=CONVERT"><img src="https://img.shields.io/badge/PDF-6583B3?style=for-the-badge&logo=DocuSign"></a> |