# Firefox

## Deleting browsing history in Firefox

![Print screen for deleting browser history in Firefox steps 1 and 2](../img/rules/firefox/en/1.png)
![Print screen for deleting browser history in Firefox step 3](../img/rules/firefox/en/2.png)
![Print screen for deleting browser history in Firefox steps 4 and 5](../img/rules/firefox/en/3.png)

1. Click on the Firefox menu button.
2. Select the **History** menu item.
3. Click on the **Delete recent history** menu item. (Shortcut: Ctrl + Alt + Del)
4. Select in the dropdown list at least the period that includes the process of your electronic voting. For instance, select the **Last hour** entry.
5. Click on the **Delete now** menu item.

