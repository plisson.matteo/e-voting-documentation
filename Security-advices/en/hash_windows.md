# Instructions on verifying the JavaScript files of the e-voting portal using OpenSSL for Windows

## Verifying the Base64-SHA256 hash value of the JavaScript files using OpenSSL

- If you would like to check that the JavaScript files used by the official e-voting portal have not been altered, you can verify the relevant Base64-SHA256 hash values.
- If you do this before you cast your e-vote, you can rule out the possibility of the JavaScript files being manipulated or falsified (replaced).
- The following description is based on the JavaScript file **platformRootCA.js**, though it may also be used for other JavaScript files.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Suchen Sie auf dem Abstimmungsportal den **Base64-SHA256-Hash** der JavaScript-Dateien **platformRootCA.js**:
![img](../img/rules/edge/de/21.png)
2. Make the **OpenSSL** software library accessible:
![img](../img/rules/edge/de/32.png)
   - as **GitLab source code** or
   - as an operating system-specific **compilation** (e.g. openssl 3.0.0 for Windows)

3. Open the **browser** and then open the browser’s **Developer tools** in one of the following ways:

   - Press **F12** on the keyboard or
   - Press **Ctrl+Shift+I** on the keyboard
   - Click on the ... icon, and then on **More tools** , followed by **Developer tools**

![img](../img/rules/edge/de/33.png)

4. Click on the Sources tab, then on the entry **platformRootCA.js.**.
   - Download the JavaScript file **platformRootCA.js** by clicking on the right-hand mouse button and selecting **Save to** , e.g. the desktop:
![img](../img/rules/edge/de/34.png)
5. In the file explorer, open the directory **openssl-3.0.0\openssl-3\x64\bin**.

   - In this bin directory you will find the OpenSSL executable **openssl.exe**.
   - Move the **platformRootCA.js** JavaScript file you have downloaded to this bin directory:
![img](../img/rules/edge/de/35.png)

6. Open **PowerShell** in this file explorer view as follows: **File > Open Windows PowerShell:** (this way you ensure PowerShell is in the right pathway, i.e. the aforementioned bin directory)
![img](../img/rules/edge/de/36.png)
7. Enter the following PowerShell code in the PowerShell editor (via copy and paste):

```powershell
Invoke-Expression ".\openssl.exe dgst -sha256 -binary platformRootCA.js | .\openssl.exe enc -base64"
```

The **PowerShell code** contains two (pipe) OpenSSL commands that are run sequentially:

- **SHA256 binary checksum** (dgst - digest - Auszug)
- **Base64 encoding** (enc - encode - Umwandlung)
![img](../img/rules/edge/de/37.png)

8. Execute the PowerShell code by pressing the Enter key:
![img](../img/rules/edge/de/38.png)
9. Compare the hash value of the PowerShell result with the hash value of the portal:
   **If both the strings are identical, it is highly likely the JavaScript file has not been altered.**
![img](../img/rules/edge/de/39.png)
