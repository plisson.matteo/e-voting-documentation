# Internet Explorer

## Certificate authentication

![Print screen for certificate authentication in Internet Explorer steps 1 and 2](../img/rules/internetexplorer/ie/en/z1.png)
![Print screen for certificate authentication in Internet Explorer step 3](../img/rules/internetexplorer/ie/en/z2.png)

1. Click on the **lock symbol** in the address line.
2. Click on **Display certificate**.
3. Scroll to the end and compare the **fingerprint** with that on your voting card.