# Google Chrome

## Deleting the browser history in Google Chrome

![Print screen for deleting browser history in Google Chrome steps 1, 2 and 3](../img/rules/chrome/en/1.png)

1. Click on the button in the Google Chrome menu.
2. Select the **More tools** menu item.
3. Select the **Delete browser data** menu item. Shortcut: Ctrl + Alt + Del

![Print screen for deleting browser history in Google Chrome steps 4, 5 and 6](../img/rules/chrome/en/2.png)

4. Select in the dropdown list at least the period that includes the process of your electronic voting. For instance, select the **Last hour** entry.
5. Tick all the checkboxes.
6. Click on the **Delete browser data** menu item.