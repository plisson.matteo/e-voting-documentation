# Instructions for checking the web address of the e-voting portal

Verification of the digital thumbprint in the browser

- If you want to verify that you are on the correct, official e-voting portal, you can check the digital thumbprint in the browser.
- If you do this before casting your vote electronically, you can rule out the possibility that you are on a manipulated or falsified website.

## Microsoft Edge

![img](../img/rules/edge/en/01.png)
![img](../img/rules/edge/en/11.png)
![img](../img/rules/edge/en/12.png)
![img](../img/rules/edge/en/13.png)
![img](../img/rules/edge/en/14.png)
![img](../img/rules/edge/en/15.png)
![img](../img/rules/edge/en/16.png)

1. Take your **voting card** and look for the **digital thumbprint**.
2. Open the **browser**, click the **lock** in the address bar, and then click **Connection is secure**:.
3. Click on the **certificate** icon.
4. Click on the **Details** tab.
5. Scroll to the **Thumbprint** entry and click on it.
6. Compare the **thumbprint** displayed with the thumbprint on the voting card. **If the two character strings are identical, you are on the correct e-voting portal** (you can ignore colons).