# Apple Safari

## Certificate authentication

![Print screen certificate authentication in Safari Explorer step 1](../img/rules/safari/en/z1.png)
![Print screen certificate authentication in Safari Explorer step 2](../img/rules/safari/en/z2.png)
![Print screen certificate authentication in Safari Explorer step 3](../img/rules/safari/en/z3.png)

1. Click on the lock symbol **in the address line**.
2. Select **Show certificate**.
3. Scroll to the end of the window until you see **fingerprints** and compare this code with that on your voting card.