# Google Chrome Mobile

## Browserverlauf löschen in Google Chrome Mobile unter iOS

![Print Screen Browserverlauf löschen in Google Chrome Mobile Schritt 1](../img/rules/chrome/de/4.png)
![Print Screen Browserverlauf löschen in Google Chrome Mobile Schritt 3](../img/rules/chrome/de/6.png)
![Print Screen Browserverlauf löschen in Google Chrome Mobile Schritt 4](../img/rules/chrome/de/7.png)

1. Klicken Sie auf die Google Chrome Menuschaltfläche und wählen Sie den Menüpunkt Einstellungen.
2. Klicken Sie auf den Menüpunkt **Datenschutz**.
3. Wählen Sie den Menüpunkt **Browserdaten löschen**.
4. Klicken Sie auf die Menüschaltfläche **Browserdaten löschen**.