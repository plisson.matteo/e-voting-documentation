# Edge Mobile

## Browserverlauf löschen in Microsoft Edge

![Print Screen Browserverlauf löschen in Microsoft Edge Schritt 1 und 2](../img/rules/internetexplorer/em/de/1.png)
![Print Screen Browserverlauf löschen in Microsoft Edge Schritt 3](../img/rules/internetexplorer/em/de/2.png)
![Print Screen Browserverlauf löschen in Microsoft Edge Schritt 4](../img/rules/internetexplorer/em/de/3.png)

1. Klicken Sie auf die Menüschaltfläche vom Edge.
2. Wählen Sie den Menüpunkt **Einstellungen.**
3. Klicken Sie auf den Menüpunkt **zu löschende Elemente**.
4. Wählen Sie die Kontrollkästchen "Cookies und gespeicherte Websitedaten" sowie "Zwischengespeicherte Daten und Dateien" an und klicken Sie auf den Menüpunkt **Löschen**.