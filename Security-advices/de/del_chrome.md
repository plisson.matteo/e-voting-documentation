# Google Chrome

## Browserverlauf löschen in Google Chrome

![Print Screen Browserverlauf löschen in Google Chrome Schritt 1, 2 und 3](../img/rules/chrome/de/1.png)

1. Klicken Sie auf die Schaltfläche im Google Chrome-Menü.
2. Wählen Sie den Menüpunkt **"Weitere Tools"**.
3. Wählen Sie den Menüpunkt **"Browserdaten löschen"**. Tastenkombination: Strg + Umschalt + Entf

![Print Screen Browserverlauf löschen in Google Chrome Schritt 4, 5 und 6](../img/rules/chrome/de/2.png)

4. Wählen Sie in der Dropdown-Liste mindestens den Zeitraum, der den Vorgang Ihrer elektronischen Stimmabgabe umfasst. Wählen Sie zum Beispiel den Eintrag **"Letzte Stunde"**.
5. Wählen Sie alle Kontrollkästchen an.
6. Klicken Sie auf die Menütaste **"Browserdaten löschen"**.

