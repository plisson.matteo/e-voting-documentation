# Internet Explorer

## Browserverlauf löschen in Internet Explorer

![Print Screen Browserverlauf löschen in Microsoft Edge Schritt 1 und 2](../img/rules/internetexplorer/e/de/1.png)
![Print Screen Browserverlauf löschen in Microsoft Edge Schritt 3](../img/rules/internetexplorer/e/de/2.png)
![Print Screen Browserverlauf löschen in Microsoft Edge Schritt 4](../img/rules/internetexplorer/e/de/3.png)

1. Klicken Sie auf die Menüschaltfläche vom Internet Explorer.
2. Wählen Sie den Menüpunkt **Einstellungen.**
3. Klicken Sie auf den Menüpunkt **zu löschende Elemente**.
4. Wählen Sie die Kontrollkästchen "Cookies und gespeicherte Websitedaten" sowie "Zwischengespeicherte Daten und Dateien" an und klicken Sie auf den Menüpunkt **Löschen**.