# Microsoft Edge Mobile

## Supprimer l’historique du navigateur dans Microsoft Edge

![Capture d’écran de la suppression de l’historique du navigateur dans Microsoft Edge, étapes 1 et 2](../img/rules/internetexplorer/em/fr/1.png)
![Capture d’écran de la suppression de l’historique du navigateur dans Microsoft Edge, étape 3](../img/rules/internetexplorer/em/fr/2.png)
![Capture d’écran de la suppression de l’historique du navigateur dans Microsoft Edge, étape 4](../img/rules/internetexplorer/em/fr/3.png)

1. Cliquez sur Edge.
2. Sélectionnez l’option **Paramètres.**
3. Cliquez sur **Choisir les éléments à effacer**.
4. Cochez les cases «Cookies et données du site web enregistrées» et «Fichiers et données en cache» puis cliquez sur le bouton **Effacer**.
