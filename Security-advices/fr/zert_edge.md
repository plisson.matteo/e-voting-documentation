# Instructions pour vérifier l'adresse Web du portail de vote électronique

Vérification de l'empreinte numérique dans le navigateur

- Si vous souhaitez vérifier que vous êtes sur le bon portail officiel de vote électronique, vous pouvez vérifier l'empreinte numérique dans le navigateur.
- Si vous le faites avant de voter par voie électronique, vous pouvez exclure la possibilité que vous vous trouviez sur un site Web manipulé ou falsifié.

## Microsoft Edge

![img](../img/rules/edge/fr/01.png)
![img](../img/rules/edge/fr/11.png)
![img](../img/rules/edge/fr/12.png)
![img](../img/rules/edge/fr/13.png)
![img](../img/rules/edge/fr/14.png)
![img](../img/rules/edge/fr/15.png)
![img](../img/rules/edge/fr/16.png)

1. Prenez votre **carte d'électeur** et recherchez l'**empreinte numérique**.
2. Ouvrez le **navigateur**, cliquez sur le **cadenas** dans la barre d'adresse, puis cliquez sur **La connexion est sécurisée**.
3. Cliquez sur l'icône du **certificat**
4. Cliquez sur l'onglet **Détails**.
5. Faites défiler jusqu'à l'entrée d'**Empreinte numérique** et cliquez dessus.
6. Comparez l'**empreinte numérique** affichée avec l'empreinte numérique sur la carte de vote. **Si les deux chaînes de caractères sont identiques, vous êtes sur le bon portail de vote électronique** (vous pouvez ignorer les deux-points).
