# Google Chrome

## Vérification du certificat

![Capture d’écran de la vérification du certificat dans Chrome, étapes 1 et 2](../img/rules/chrome/fr/z1.png)
![Capture d’écran de la vérification du certificat dans Chrome, étape 3](../img/rules/chrome/fr/z2.png)

1. Cliquez sur l’icône **«Sécurisé»**.
2. Cliquez sur **«Valide»** sous Certificat.
3. Sélectionnez l’onglet **«Détails»**
4. Cliquez sur le champ **«Empreinte numérique»** et comparez la valeur indiquée avec celle figurant sur votre carte de vote. Elles doivent être identiques.