# Google Chrome

## Cancellare la cronologia di navigazione in Google Chrome

![Printscreen della cancellazione della cronologia di navigazione in Google Chrome passo 1, 2 e 3](../img/rules/chrome/it/1.png)

1. Cliccare sul simbolo nel menu di Google Chrome.
2. Selezionare il punto del menu **«Altri strumenti»**.
3. Selezionare il punto del menu **«Cancella i dati di navigazione»**. Combinazione di tasti: Ctrl + Shift + Del

![Printscreen della cancellazione della cronologia di navigazione in Google Chrome passo 4, 5 e 6](../img/rules/chrome/it/2.png)

4. Selezionare nel menu a tendina almeno l’intervallo di tempo che comprende il processo del vostro voto elettronico. Selezionare per esempio la voce **«Ultima ora»**.
5. Selezionare tutte le caselle di controllo.
6. Cliccare sul tasto del menu **«Cancella cronologia di navigazione»**.
