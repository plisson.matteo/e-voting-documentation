# Google Chrome

## Verifitgar il certificat

![Printscreen co verifitgar il certificat en Google Chrome pass 1 e 2](../img/rules/chrome/de/z1.png)
![Printscreen co verifitgar il certificat en Google Chrome pass 3](../img/rules/chrome/de/z2.png)

1. Cliccai sin il buttun **"Mussar las infurmaziuns da la pagina-web"**.
2. Cliccai sin **"Valaivel"** sut il certificat.
3. Tscherni il register **"Detagls"**.
4. Cliccai sin il champ **"Impronta dal det"** e cumparegliai la valur cun quella sin Voss attest da votar. Ella sto esser identica.
