# Instrucziun per controllar l'adressa-web dal portal dad e-voting

Examinar l'impronta digitala (fingerprint) en il navigatur

- Sche Vus vulais controllar che Vus sajas sin il portal dad e-voting correct ed uffizial, pudais Vus examinar l'impronta digitala en il navigatur.
- Sche Vus faschais questa controlla avant che vuschar electronicamain, pudais Vus excluder dad esser sin ina website manipulada u falsifitgada.

## Microsoft Edge

![img](../img/rules/edge/de/01.png)
![img](../img/rules/edge/de/11.png)
![img](../img/rules/edge/de/12.png)
![img](../img/rules/edge/de/13.png)
![img](../img/rules/edge/de/14.png)
![img](../img/rules/edge/de/15.png)
![img](../img/rules/edge/de/16.png)

1. Prendai Voss **attest da votar** e tschertgai **l'impronta digitala (fingerprint)**.
2. Avri il **navigatur**, cliccai sin il **marschlos** en la lingia d'adressa e lura sin **"Verbindung ist sicher"**:.
3. Cliccai sin il simbol dal **certificat**.
4. Cliccai sin il register dals **detagls**.
5. Scrollai engiu fin tar **l'impronta** e cliccai sin quella.
6. Cumparegliai **l'impronta** visualisada cun l'impronta sin l'attest da votar: **Sche las duas successiuns da segns èn identicas, essas Vus sin il dretg portal dad e-voting** (puncts dubels pudais Vus ignorar).
