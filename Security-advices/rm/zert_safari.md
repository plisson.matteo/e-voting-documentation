# Apple Safari

## Verifitgar il certificat

![Printscreen co verifitgar il certificat en Apple Safari pass 1](../img/rules/safari/de/z1.png)
![Printscreen co verifitgar il certificat en Apple Safari pass 2](../img/rules/safari/de/z2.png)
![Printscreen co verifitgar il certificat en Apple Safari pass 3](../img/rules/safari/de/z3.png)

1. Cliccai sin il marschlos dal navigatur **en la lingia d'adressa**.
2. Elegiai **"Zertifikat einblenden"**.
3. Scrollai enfin il pli giudim, nua che Vus vesais **l'impronta dal det** e cumparegliai quel code cun il code notà sin Voss attest da votar.
