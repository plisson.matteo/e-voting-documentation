# Internet Explorer

## Verifitgar il certificat

![Printscreen co verifitgar il certificat en l'Internet Explorer pass 1 e 2](../img/rules/internetexplorer/ie/de/z1.png)
![Printscreen co verifitgar il certificat en l'Internet Explorer pass 3](../img/rules/internetexplorer/ie/de/z2.png)

1. Cliccai sin il **simbol dal marschlos** en la lingia d'adressa.
2. Cliccai sin **"Zertifikat anzeigen"**.
3. Scrollai enfin giudim e cumparegliai **l'impronta dal det** cun quella sin Voss attest da votar.
