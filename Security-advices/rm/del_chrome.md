# Google Chrome

## Stizzar la cronologia dal navigatur en Google Chrome

![Printscreen co stizzar la cronologia dal navigatur en Google Chrome pass 1, 2 e 3](../img/rules/chrome/de/1.png)

1. Cliccai sin il menu da Google Chrome.
2. Elegiai **"Weitere Toolss"**.
3. Selecziunai **"Browserdaten löschen"**. Cumbinaziun da tastas: Strg + Umschalt + Entf
![Printscreen co stizzar la cronologia dal navigatur en Google Chrome pass 4, 5 e 6](../img/rules/chrome/de/2.png)
4. Elegiai en la glista da defilar almain il spazi da temp che cumpiglia il mument, cura che Vus avais votà electronicamain. Tscherni per exempel l'endataziun **"Letzte Stunde"**.
5. Selecziunai tut las chaschettas da controlla.
6. Cliccai sin **"Browserdaten löschen"**.
