# Instrucziun per controllar las datotecas da Javascript dal portal per la votaziun electronica cun agid dad OpenSSL per Windows

## Controllar la valur hash Base64-SHA256 da las datotecas da Javascript cun agid dad OpenSSL

- Sche Vus vulais controllar che las datotecas da Javascript utilisadas dal portal per la votaziun electronica uffizial na sajan betg vegnidas modifitgadas, pudais Vus verifitgar las valurs hash Base64-SHA256 respectivas.
- Sche Vus faschais questa controlla avant che vuschar electronicamain, pudais Vus excluder che las datotecas da Javascript èn vegnidas manipuladas u falsifitgadas (remplazzadas).
- La descripziun sutvart vala per la datoteca da Javascript **platformRootCA.js**.Ins po però era duvrar ella per autras datotecas da Javascript.

### Microsoft Edge

![img](../img/rules/edge/de/01.png)

1. Tschertgai sin il portal da votaziun il hash Base64-SHA256 da las datotecas da Javascript **platformRootCA.js**:
![img](../img/rules/edge/de/21.png)
2. Avri la software OpenSSL:
![img](../img/rules/edge/de/32.png)o sco **code da funtauna** da GitLab ubain
  - sco **cumpilaziun** specifica dal sistem operativ (p.ex. openssl-3.0.0 per Windows)

3. Avri il **navigatur** e suenter ils **tools da svilup** dal navigatur. Quai pudais Vus far en ina da las suandantas modas:
- smatgar **F12** sin la tastatura ubain
- smatgar **Ctrl+Shift+I** sin la tastatura ubain
- cliccar sin ils **trais puncts**, suenter sin Weitere Tools e la finala sin **Entwicklungstools**

![img](../img/rules/edge/de/33.png)

4. Cliccai sin il register **Quellen** e sin l'opziun **platformRootCA.js**.
   Chargiai giu la datoteca da Javascript **platformRootCA.js** cun cliccar sin la tasta dretga da la mieur e selecziunar **Speichern unter** . Memorisai la datoteca p.ex. sin il desktop:
![img](../img/rules/edge/de/34.png)

5. Avri en l'explorer da datotecas il register **openssl-3.0.0\openssl-3\x64\bin**.
- En quest ordinatur bin sa chatta la datoteca exequibla dad OpenSSL **openssl.exe**.
- Transferi la datoteca da Javascript telechergiada **platformRootCA.js** en quest register bin:![img](../img/rules/edge/de/35.png)

6. Cliccai en questa vista da l'explorer da datotecas sin **Datei** e **Windows PowerShell öffnen** per avrir **PowerShell**
   (Uschia sa chatta PowerShell gist en la dretga via d'access, qvd. en il register bin numnà survart.)
![img](../img/rules/edge/de/36.png)

7. Scrivai il suandant code da PowerShell en l'editur da PowerShell (copiar ed encollar):

```powershell
Invoke-Expression ".\openssl.exe dgst -sha256 -binary platformRootCA.js | .\openssl.exe enc -base64"
```

Il code da PowerShell cuntegna dus cumonds consecutivs (pipe) dad OpenSSL:

- **SHA256 binary checksum** (dgst - digest - extrair)
- **Base64 encoding** (enc - encode - transfurmar)
![img](../img/rules/edge/de/37.png)

8. Exequi il code da PowerShell cun cliccar sin Enter:
![img](../img/rules/edge/de/38.png)

9. Cumparegliai la **valur hash** dal resultat da PowerShell cun la valur hash dal portal:
   **Sche las duas successiuns da segns èn identicas, n'è la datoteca da Javascript betg vegnida modifitgada.**
![img](../img/rules/edge/de/39.png)
