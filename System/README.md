# [Architecture of the Swiss Post Voting System](SwissPost_Voting_System_architecture_document.pdf)

## What is the content of this document?

Architecture documentation of the e-voting system based on the arc42 architecture documentation style. In case this is unfamiliar the arc42 site provides a concise overview and examples which are worth taking a moment to review.

# [Verifier of the Swiss Post Voting System](Verifier_Specification.pdf)

## What is the content of this document?

The Swiss Post Voting System requires a verification software—the verifier—to verify the cryptographic evidence. The specification and development of the verifier goes hand in hand with the Swiss Post Voting System, and the verifier challenges and extensively tests a protocol run.

# [Specification of the Swiss Post Voting System](System_Specification.pdf)

## What is the content of this document?

The system specification provides a detailed specification of the cryptographic protocol — from the configuration phase to the voting phase to the tally phase — and includes pseudo-code representations of most algorithms. The document has the following target audiences:

* developers implementing the Swiss Post Voting System.
* reviewers checking that the system specification matches the  [computational proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol).
* reviewers checking that the implementation (source code) matches the system specification.

## Why is a detailed specification of the cryptographic protocol important?

[Haenni et al](https://arbor.bfh.ch/13834/) stressed the importance of a precise system specification—using mathematical formalism and pseudo-code descriptions of algorithms—to reduce implementation errors and increase auditability of an e-voting system.

The system specification details all relevant cryptographic operations of the cryptographic protocol and communications between protocol participants. The pseudo-code representations are sufficiently precise to leave no room for interpretation while being independent of any programming language.

Moreover, the system specification links the [computational proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol) which formally proves verifiability and vote secrecy to the actual source code.

## Changes since publication 2019

We have improved the system specification in multiple ways since the initial publication of 2019:

* Completeness: We provide explicit algorithms for representing and converting basic data types.
* Clarity: Pseudo-code algorithms detail the domain of input and output algorithms and provide a mathematically precise description of the operations.
* Consistency: Method and variable names align more closely to the [computational proof](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol).
* Structure: The specification describes low-level algorithms separately and references them in high-level algorithms; therefore avoiding repetition.
* Readability: Sequence diagrams and overview tables increase the specificiation's readability.
* Separation of concerns: Each chapter focuses on a main aspect of the protocol. For instance, we moved the description on channel security (using digital signatures) to a separate chapter.

## Changes in version 1.0.0

Version 1.0.0 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts. The experts' reports can be found under [Reports](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Reports/Examination2021). We want to thank the experts for their high-quality, constructive remarks:

* Vanessa Teague (Thinking Cybersecurity), Olivier Pereira (Université catholique Louvain), Thomas Edmund Haines (Australian National University)
* Aleksander Essex (Western University Canada)
* Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis (Bern University of Applied Sciences)

Version 1.0.0 of the system specification contains the following changes:

* Specified the Tally control component's VerifyVotingClientProofs algorithm (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Specified the derivation of the Electoral Board secret key from their passwords.
* Replaced PBKDF with Argon2 (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
* Renamed DecodePlaintexts to ProcessPlaintexts and GetMixnetInitialPayload to GetMixnetInitialCiphertexts.
* Detailed some consistency checks.
* Minor corrections.

## Changes in version 0.9.9

Version 0.9.9 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above).

Version 0.9.9 of the system specification strengthens the cryptographic protocol by shifting responsibilities from the auditors and the voting server to the control components.
Namely, each control component verifies the correctness of a confirmation attempt, and each control component verifies the preceding shuffle and decryption proofs.
Making the control components perform additional validations prevents attacks against privacy, as highlighted by Cortier, Gaudry, and Debant (see [Gitlab issue #11](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/11)) without relying on a trustworthy auditor
and prevents inconsistent views across control components.

Moreover, Version 0.9.9 of the system specification contains the following changes:

* Replaced the random value generation algorithms with crypto-primitive methods.
* Explained operations on lists and key-value maps (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
* Introduced a usability section justifying the length of the Start Voting Key and the number of PBKDF iterations (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
* Extended the channel security section with tables detailing the context for every exchanged message (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
* Highlighted the MixOnline protocol with a separate sequence diagram.
* Ensured that the GenCMTable algorithm produces distinct Choice Return Codes for each voting option per voter.
* Removed the Shamir Secret Sharing algorithms since we no longer require this method in the strengthened cryptographic protocol.
* Removed the SecureLogs algorithms since we no longer require the SecureLogs in the strengthened cryptographic protocol version.
* Clarified minor points in the sequence diagrams.
* Fixed minor mistakes in various algorithms.

## Changes in version 0.9.8

Version 0.9.8 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes:

* Removed key compression in favor of dropping excess public keys (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Replaced the KeyDerivationFunction and DeriveKey method with the KDF and KDFtoZq algorithms (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Minor corrections in the DecryptPCC and MixDecOffline algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
* Fixed minor mistakes and typos in multiple algorithms.

## Changes in version 0.9.7

Version 0.9.7 of the system specification incorporates some of the feedback from the Federal Chancellery's mandated experts (see above) and contains the following changes:

* Updated the system specification to the new version of the Ordinance on Electronic Voting (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Specified the generation of SecureLogs. Carsten Schürmann raised this issue in his maturity analysis of the system documentation (see Gitlab issue [#9](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/9)).
* Added a HashAndSquare operation before exponentiating the Ballot Casting Key to facilitate the security reduction in the computational proof (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines).
* Specified a GetMixnetInitialPayload algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis).
* Removed the BasicDataTypes section.
* Clarified the Testing-only security level.
* Fixed the SendVote diagram.
* Fixed an incorrect verification in the DecryptPCC_j algorithm (thanks to Marc Wyss from ETHZ).
* Fixed some minor errors in the CreateLCCShare and CreateLVCCShare algorithm.
* Fixed minor mistakes and typos in the document.

## Changes in version 0.9.6

Version 0.9.6 of the system specification contains the following changes:

* Strengthened  the protocol to verify sent-as-intended properties and vote correctness online in the control components. See Gitlab issue [#7](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/7) for further explanations.
* Specified a recovery scenario for incorrectly inserted or discarded votes (tally phase). See Gitlab issue [#8](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/8) for further explanations.
* Expanded the section on two-round return code schemes with alternative scenarios.
* Explained the election context and its relevance to the protocol's algorithms.
* Moved the GetEncryptionParameters algorithm to the crypto-primitives specification.
* Streamlined the usage and name of lists between the computational proofs and the system specifications.
* Improved clarity of sequence diagrams.
* Fixed smaller typos and errors in various sections.

## Known Issues

The current version of the system specification contains the following known issues:

* Missing description of how we encode and decode write-in candidates and include them as part of a multi-recipient ElGamal ciphertext.

## Future Work

In future versions, we plan to address the following issue:

* Performance optimizations such as fixed-base exponentiations in the setup component.
* Voter authentication. Describe how the voting client authenticates to the voting server.

## Limitations

We would like to mention the following limitations of the system specification:

* The system specification does **not** provide a formal proof of verifiability and vote secrecy. Please check the [computational proof document](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol) if you are looking for a security analysis.
* The system specification is bound to the [same limitations](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/tree/master/Protocol#limitations) than the computational proof regarding quantum computers, a trustworthy voting client for vote secrecy, and a trustworthy setup and printing component.
* The system specification does **not** describe which programming language and frameworks implement the protocol, the principles and design patterns that the source code follows, and the technical and organizational constraints that the solution is bound to. These aspects will be covered in the architecture documentation.
* The system specification does **not** detail the verifier's algorithms, which will be described in the verifier specification.